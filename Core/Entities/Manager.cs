using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Entities;

namespace Core.Entities
{
    public class Manager : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Manager UpManager { get; set; }
        public int? UpManagerId { get; set; }
        public ICollection<Manager> SubManagers { get; } = new List<Manager>();
        public DateTime RegistrationDate { get; set; }
        public int SoldItemsNumber { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal SoldItemsPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal WorkedOut { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal ReferalsWorkedOut { get; set; }
        public decimal GetTotal()
        {
            return WorkedOut + ReferalsWorkedOut;
        }
        public string AppUserId { get; set; }
        public bool IsDeleted { get; set; } = false;

    }
}