using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Core.Specifications
{
    public class ProductsSpecification : BaseSpecification<Product>
    {
        public ProductsSpecification(ProductSpecParams productParams) : base(x =>
              (string.IsNullOrEmpty(productParams.Search) || x.Name.ToLower().Contains(productParams.Search)) &&
              (!productParams.PriceFrom.HasValue || x.Price >= productParams.PriceFrom) &&
              (!productParams.PriceTo.HasValue || x.Price <= productParams.PriceTo) &&
              (!productParams.QuantityFrom.HasValue || x.Quantity >= productParams.QuantityFrom) &&
              (!productParams.QuantityTo.HasValue || x.Quantity <= productParams.QuantityTo)
        )
        {
            AddInclude(x => x.Include(e => e.Manager));
           AddOrderByDescending(x => x.AddedDate);
            ApplyPaging(productParams.PageSize * (productParams.PageIndex - 1), productParams.PageSize);

            if (!string.IsNullOrEmpty(productParams.Sort))
            {
                switch (productParams.Sort)
                {
                    case "name":
                        AddOrderBy(p => p.Name);
                        break;
                    case "priceAsc":
                        AddOrderBy(p => p.Price);
                        break;
                    case "priceDesc":
                        AddOrderByDescending(p => p.Price);
                        break;
                    case "quantityAsc":
                        AddOrderBy(p => p.Quantity);
                        break;
                    case "quantityDesc":
                        AddOrderByDescending(p => p.Quantity);
                        break;
                    default:
                        AddOrderBy(n => n.AddedDate);
                        break;
                }
            }
        }
        public ProductsSpecification(int id) : base(x => x.Id == id)
        {
            AddInclude(x => x.Include(e => e.Manager));
        }
    }
}