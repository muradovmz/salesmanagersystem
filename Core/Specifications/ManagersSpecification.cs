using Core.Entities;

namespace Core.Specifications
{
    public class ManagersSpecification : BaseSpecification<Manager>
    {
        public ManagersSpecification(string appUserId) : base(x => x.AppUserId == appUserId)
        {

        }

        public ManagersSpecification(int managerId) : base(x => x.Id == managerId)
        {

        }

        public ManagersSpecification(ManagerSpecParams managerParams) : base(x =>
              (string.IsNullOrEmpty(managerParams.SearchFirstName) || x.FirstName.ToLower().Contains(managerParams.SearchFirstName)) &&
              (string.IsNullOrEmpty(managerParams.SearchLastName) || x.LastName.ToLower().Contains(managerParams.SearchLastName)) &&
              (!managerParams.SoldItemNumberFrom.HasValue || x.SoldItemsNumber >= managerParams.SoldItemNumberFrom) &&
              (!managerParams.SoldItemNumberTo.HasValue || x.SoldItemsNumber <= managerParams.SoldItemNumberTo) &&
              (!managerParams.SoldItemPriceFrom.HasValue || x.SoldItemsPrice >= managerParams.SoldItemPriceFrom) &&
              (!managerParams.SoldItemPriceTo.HasValue || x.SoldItemsPrice <= managerParams.SoldItemPriceTo) &&
              (!managerParams.WorkedOutFrom.HasValue || x.WorkedOut >= managerParams.WorkedOutFrom) &&
              (!managerParams.WorkedOutTo.HasValue || x.WorkedOut <= managerParams.WorkedOutTo) &&
              (!managerParams.ReferalsWorkedOutFrom.HasValue || x.ReferalsWorkedOut >= managerParams.ReferalsWorkedOutFrom) &&
              (!managerParams.ReferalsWorkedOutTo.HasValue || x.ReferalsWorkedOut <= managerParams.ReferalsWorkedOutTo) &&
              (!managerParams.TotalFrom.HasValue || (x.WorkedOut + x.ReferalsWorkedOut) >= managerParams.TotalFrom) &&
              (!managerParams.TotalTo.HasValue || (x.WorkedOut + x.ReferalsWorkedOut) <= managerParams.TotalTo) &&
              (!(managerParams.RegisteredFrom != null) || x.RegistrationDate > managerParams.RegisteredFrom) &&
              (!(managerParams.RegisteredTo != null) || x.RegistrationDate < managerParams.RegisteredTo) &&
              (x.IsDeleted != true) && (x.Id!=1)
              
        )
        {
            AddOrderByDescending(x => x.RegistrationDate);
            ApplyPaging(managerParams.PageSize * (managerParams.PageIndex - 1), managerParams.PageSize);

            if (!string.IsNullOrEmpty(managerParams.Sort))
            {
                switch (managerParams.Sort)
                {
                    case "firstnameAsc":
                        AddOrderBy(p => p.FirstName);
                        break;
                    case "lastnameAsc":
                        AddOrderBy(p => p.LastName);
                        break;
                    case "soldItemsNumberAsc":
                        AddOrderBy(p => p.SoldItemsNumber);
                        break;
                    case "soldItemsNumberDesc":
                        AddOrderByDescending(p => p.SoldItemsNumber);
                        break;
                    case "soldItemsPriceAsc":
                        AddOrderBy(p => p.SoldItemsPrice);
                        break;
                    case "soldItemsPriceDesc":
                        AddOrderByDescending(p => p.SoldItemsPrice);
                        break;
                    case "workedOutAsc":
                        AddOrderBy(p => p.WorkedOut);
                        break;
                    case "workedOutDesc":
                        AddOrderByDescending(p => p.WorkedOut);
                        break;
                    case "referalsWorkedOutAsc":
                        AddOrderBy(p => p.ReferalsWorkedOut);
                        break;
                    case "referalsWorkedOutDesc":
                        AddOrderByDescending(p => p.ReferalsWorkedOut);
                        break;
                    case "totalAsc":
                        AddOrderBy(x => (x.WorkedOut + x.ReferalsWorkedOut));
                        break;
                    case "totalDesc":
                        AddOrderByDescending(x => (x.WorkedOut + x.ReferalsWorkedOut));
                        break;
                    default:
                        AddOrderBy(n => n.RegistrationDate);
                        break;
                }
            }
        }
    }
}