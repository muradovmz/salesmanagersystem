using System;

namespace Core.Specifications
{
    public class ManagerSpecParams
    {
        private const int MaxPageSize = 50;
        public int PageIndex { get; set; } = 1;
        private int _pageSize = 5;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

        public int? SoldItemNumberFrom { get; set; }
        public int? SoldItemNumberTo { get; set; }
        public decimal? SoldItemPriceFrom { get; set; }
        public decimal? SoldItemPriceTo { get; set; }
        public decimal? WorkedOutFrom { get; set; }
        public decimal? WorkedOutTo { get; set; }
        public decimal? ReferalsWorkedOutFrom { get; set; }
        public decimal? ReferalsWorkedOutTo { get; set; }
        public decimal? TotalFrom { get; set; }
        public decimal? TotalTo { get; set; }
        public string Sort { get; set; }
        public DateTime? RegisteredFrom { get; set; }
        public DateTime? RegisteredTo { get; set; }
        private string _searchFirstName;
        public string SearchFirstName
        {
            get => _searchFirstName;
            set => _searchFirstName = value.ToLower();
        }
        private string _searchLastName;
        public string SearchLastName
        {
            get => _searchLastName;
            set => _searchLastName = value.ToLower();
        }
    }
}