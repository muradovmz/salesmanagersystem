using Core.Entities;

namespace Core.Specifications
{
    public class ManagerWithFiltersForCountSpecification : BaseSpecification<Manager>
    {
        public ManagerWithFiltersForCountSpecification(ManagerSpecParams managerParams) : base(x =>
               (string.IsNullOrEmpty(managerParams.SearchFirstName) || x.FirstName.ToLower().Contains(managerParams.SearchFirstName)) &&
               (string.IsNullOrEmpty(managerParams.SearchLastName) || x.LastName.ToLower().Contains(managerParams.SearchLastName)) &&
               (!managerParams.SoldItemNumberFrom.HasValue || x.SoldItemsNumber >= managerParams.SoldItemNumberFrom) &&
               (!managerParams.SoldItemNumberTo.HasValue || x.SoldItemsNumber <= managerParams.SoldItemNumberTo) &&
               (!managerParams.SoldItemPriceFrom.HasValue || x.SoldItemsPrice >= managerParams.SoldItemPriceFrom) &&
               (!managerParams.SoldItemPriceTo.HasValue || x.SoldItemsPrice <= managerParams.SoldItemPriceTo) &&
               (!managerParams.WorkedOutFrom.HasValue || x.WorkedOut >= managerParams.WorkedOutFrom) &&
               (!managerParams.WorkedOutTo.HasValue || x.WorkedOut <= managerParams.WorkedOutTo) &&
               (!managerParams.ReferalsWorkedOutFrom.HasValue || x.ReferalsWorkedOut >= managerParams.ReferalsWorkedOutFrom) &&
               (!managerParams.ReferalsWorkedOutTo.HasValue || x.ReferalsWorkedOut <= managerParams.ReferalsWorkedOutTo) &&
               (!managerParams.TotalFrom.HasValue || (x.WorkedOut + x.ReferalsWorkedOut) >= managerParams.TotalFrom) &&
               (!managerParams.TotalTo.HasValue || (x.WorkedOut + x.ReferalsWorkedOut) <= managerParams.TotalTo) &&
               (!(managerParams.RegisteredFrom != null) || x.RegistrationDate > managerParams.RegisteredFrom) &&
               (!(managerParams.RegisteredTo != null) || x.RegistrationDate < managerParams.RegisteredTo) &&
               (x.IsDeleted != true) && (x.Id!=1)
        )
        {

        }
    }
}