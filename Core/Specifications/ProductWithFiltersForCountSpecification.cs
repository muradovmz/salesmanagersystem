using Core.Entities;

namespace Core.Specifications
{
    public class ProductWithFiltersForCountSpecification : BaseSpecification<Product>
    {
        public ProductWithFiltersForCountSpecification(ProductSpecParams productParams) : base(x =>
                (string.IsNullOrEmpty(productParams.Search) || x.Name.ToLower().Contains(productParams.Search)) &&
              (!productParams.PriceFrom.HasValue || x.Price >= productParams.PriceFrom) &&
              (!productParams.PriceTo.HasValue || x.Price <= productParams.PriceTo) &&
              (!productParams.QuantityFrom.HasValue || x.Quantity >= productParams.QuantityFrom) &&
              (!productParams.QuantityTo.HasValue || x.Quantity <= productParams.QuantityTo)
        )
        {

        }
    }
}