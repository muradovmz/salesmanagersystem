using System;
using System.ComponentModel.DataAnnotations;

namespace API.Dtos
{
    public class ProductCreateDto
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
        public int ManagerId { get; set; }
        public DateTime AddedDate { get; set; }
    }
}