namespace API.Dtos
{
    public class ManagerToUpdateDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}