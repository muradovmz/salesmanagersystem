using System;

namespace API.Dtos
{
    public class ManagerToReturnDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? UpManagerId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int SoldItemsNumber { get; set; } 
        public decimal SoldItemsPrice { get; set; }
        public decimal WorkedOut { get; set; }
        public decimal ReferalsWorkedOut { get; set; }
        public decimal Total {get;set;}
        public string AppUserId {get; set;}
        public bool IsDeleted { get; set; }
   
    }
}