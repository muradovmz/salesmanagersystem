namespace API.Dtos
{
    public class SellProductDto
    {
        public int ManagerId { get; set; }
        public string Quantity { get; set; }
    }
}