using System;
using System.ComponentModel.DataAnnotations;

namespace API.Dtos
{
    public class ManagerCreateDto
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public int? UpManagerId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string AppUserId { get; set; }
    }
}