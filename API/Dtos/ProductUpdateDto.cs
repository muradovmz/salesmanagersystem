using System;
using System.ComponentModel.DataAnnotations;

namespace API.Dtos
{
    public class ProductUpdateDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$",
            ErrorMessage = "Price must be a decimal (e.g 20.30)")]
        public decimal Price { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Quantity is wrong")]
        public int Quantity { get; set; }
        public string AppUserEmail { get; set; }

        public int ManagerId { get; set; }
        public DateTime AddedDate { get; set; }

    }
}