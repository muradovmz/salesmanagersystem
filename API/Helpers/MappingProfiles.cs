using API.Dtos;
using AutoMapper;
using Core.Entities;
using Core.Entities.Identity;

namespace API.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Product, ProductToReturnDto>()
                .ForMember(d => d.ManagerId, o => o.MapFrom(s => s.Manager.Id));
            CreateMap<Manager, ManagerToReturnDto>();
            CreateMap<ManagerCreateDto, Manager>();
            CreateMap<ManagerToUpdateDto, Manager>();
            CreateMap<ProductUpdateDto, Product>();
            CreateMap<ProductCreateDto, Product>();
        }
    }
}