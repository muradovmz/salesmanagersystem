using System.Collections.Generic;
using System.Threading.Tasks;
using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ManagersController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ManagersController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<ActionResult<Pagination<ManagerToReturnDto>>> GetManagers(
            [FromQuery] ManagerSpecParams managersParams)
        {
            var spec = new ManagersSpecification(managersParams);

            var countSpec = new ManagerWithFiltersForCountSpecification(managersParams);

            var totalItems = await _unitOfWork.Repository<Manager>().CountAsync(countSpec);

            var managers = await _unitOfWork.Repository<Manager>().ListAsync(spec);

            var data = _mapper.Map<IReadOnlyList<Manager>, IReadOnlyList<ManagerToReturnDto>>(managers);

            return Ok(new Pagination<ManagerToReturnDto>(managersParams.PageIndex, managersParams.PageSize, totalItems, data));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ManagerToReturnDto>> GetManager(int id)
        {
            var spec = new ManagersSpecification(id);

            var manager = await _unitOfWork.Repository<Manager>().GetEntityWithSpec(spec);

            if (manager == null)
            {
                return NotFound(new ApiResponse(404));
            }

            return _mapper.Map<Manager, ManagerToReturnDto>(manager);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Manager>> UpdateProduct(int id, ManagerToUpdateDto managerToUpdate, [FromQuery] int executerId)
        {
            var manager = await _unitOfWork.Repository<Manager>().GetByIdAsync(id);

            if (manager.UpManagerId != executerId) return BadRequest(new ApiResponse(400, "Update process is forbidden"));

            _mapper.Map(managerToUpdate, manager);

            _unitOfWork.Repository<Manager>().Update(manager);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem updating manager"));

            return Ok(manager);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteManager(int id,[FromQuery] int executerId)
        {
            var manager = await _unitOfWork.Repository<Manager>().GetByIdAsync(id);

            if (manager.UpManagerId != executerId) return BadRequest(new ApiResponse(400, "Delete process is forbidden"));

            manager.IsDeleted=true;

            _unitOfWork.Repository<Manager>().Update(manager);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem deleting product"));

            return Ok();
        }


    }
}