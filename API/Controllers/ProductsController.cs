using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Entities.Identity;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class ProductsController : BaseApiController
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;
        public ProductsController(IUnitOfWork unitOfWork, IMapper mapper, UserManager<AppUser> userManager)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<Pagination<ProductToReturnDto>>> GetProducts(
            [FromQuery] ProductSpecParams productParams)
        {
            var spec = new ProductsSpecification(productParams);

            var countSpec = new ProductWithFiltersForCountSpecification(productParams);

            var totalItems = await _unitOfWork.Repository<Product>().CountAsync(countSpec);

            var products = await _unitOfWork.Repository<Product>().ListAsync(spec);

            var data = _mapper.Map<IReadOnlyList<Product>, IReadOnlyList<ProductToReturnDto>>(products);

            return Ok(new Pagination<ProductToReturnDto>(productParams.PageIndex, productParams.PageSize, totalItems, data));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductToReturnDto>> GetProduct(int id)
        {
            var spec = new ProductsSpecification(id);

            var product = await _unitOfWork.Repository<Product>().GetEntityWithSpec(spec);

            if (product == null)
            {
                return NotFound(new ApiResponse(404));
            }

            return _mapper.Map<Product, ProductToReturnDto>(product);
        }

        [HttpPost]
        public async Task<ActionResult<Product>> CreateProduct(ProductCreateDto productToCreate)
        {
            var newProduct = new Product
            {
                Name = productToCreate.Name,
                Price = Decimal.Parse(productToCreate.Price),
                Quantity = Int32.Parse(productToCreate.Quantity),
                ManagerId = productToCreate.ManagerId,
                AddedDate = DateTime.Now
            };

            _unitOfWork.Repository<Product>().Add(newProduct);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem creating product"));

            return Ok(newProduct);
        }


        [HttpPut("{id}")]
        public async Task<ActionResult<Product>> UpdateProduct(int id, ProductCreateDto productToUpdate)
        {
            var product = await _unitOfWork.Repository<Product>().GetByIdAsync(id);

            if (product.ManagerId != productToUpdate.ManagerId) return BadRequest(new ApiResponse(400, "Update process is forbidden"));


            product.Name = productToUpdate.Name;
            product.Price = Decimal.Parse(productToUpdate.Price);
            product.Quantity = Int32.Parse(productToUpdate.Quantity);


            _unitOfWork.Repository<Product>().Update(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem updating product"));

            return Ok(product);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProduct(int id, [FromQuery] int executerId)
        {
            var product = await _unitOfWork.Repository<Product>().GetByIdAsync(id);

            if (product.ManagerId != executerId) return BadRequest(new ApiResponse(400, "Delete process is forbidden"));

            _unitOfWork.Repository<Product>().Delete(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem deleting product"));

            return Ok();
        }


        /*   public <List<Manager> GetUpManagers(Manager manager, List<Manager> upManagers)
          {
              while (true)
              {
                  if (manager.UpManagerId == 0) return upManagers;
                  var upManager = await _unitOfWork.Repository<Manager>().GetByIdAsync(manager.UpManagerId);
                  upManagers.Add(upManager);
                  manager = upManager;
              }
          } */



        [HttpPut("sell/{id}")]
        public async Task<ActionResult> SellProduct(int id, SellProductDto soldProduct)
        {
            var product = await _unitOfWork.Repository<Product>().GetByIdAsync(id);

            Manager manager = await _unitOfWork.Repository<Manager>().GetByIdAsync(soldProduct.ManagerId);

            manager.WorkedOut += ((product.Price * Int32.Parse(soldProduct.Quantity)) * (decimal)0.1);
            manager.SoldItemsNumber += Int32.Parse(soldProduct.Quantity);
            manager.SoldItemsPrice +=product.Price*Int32.Parse(soldProduct.Quantity);

            _unitOfWork.Repository<Manager>().Update(manager);

            IList<Manager> upManagers = new List<Manager>();

            bool looping = true;

            while (looping)
            {
                if (manager.UpManagerId == 1) looping = false;
                else
                {
                    if (manager.UpManagerId != null)
                    {
                        var upManager = await _unitOfWork.Repository<Manager>().GetByIdAsync(manager.UpManagerId);
                        upManagers.Add(upManager);
                        manager = upManager;
                    }
                }
            }

            for (int i = 0; i < upManagers.Count; i++)
            {
                if (i == 0)
                {
                    upManagers[i].ReferalsWorkedOut += ((product.Price * Int32.Parse(soldProduct.Quantity)) * (decimal)0.05)*(decimal)0.4;
                    _unitOfWork.Repository<Manager>().Update(upManagers[i]);
                }
                else
                {
                    upManagers[i].ReferalsWorkedOut += ((product.Price * Int32.Parse(soldProduct.Quantity)) * (decimal)0.05)*(decimal)0.6 / (upManagers.Count-1);
                    _unitOfWork.Repository<Manager>().Update(upManagers[i]);
                }
            }


            product.Quantity -= Int32.Parse(soldProduct.Quantity);
            _unitOfWork.Repository<Manager>().Update(manager);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem selling product"));

            return Ok();
        }


    }
}