export interface IProduct {
  id: number;
  name: string;
  price: number;
  quantity: number;
  managerId: number;
}

export interface IProductToCreate {
  name: string;
  price: number;
  quantity: number;
  managerId: number;
}


