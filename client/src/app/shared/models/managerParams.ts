export class ManagerParams {
    sort = '';
    pageNumber = 1;
    pageSize = 6;
    searchByFirstName: string;
    searchByLastName: string;
    soldItemNumberFrom: string;
    soldItemNumberTo: string;
    soldItemPriceFrom: string;
    soldItemPriceTo: string;
    workedOutFrom: string;
    workedOutTo: string;
    referalsWorkedOutFrom: string;
    referalsWorkedOutTo: string;
    totalFrom: string;
    totalTo: string;
    registeredFrom: string;
    registeredTo: string;
}
