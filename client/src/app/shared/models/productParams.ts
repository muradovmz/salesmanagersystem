export class ProductParams {
    sort = '';
    pageNumber = 1;
    pageSize = 6;
    search: string;
    priceFrom: string;
    priceTo: string;
}
