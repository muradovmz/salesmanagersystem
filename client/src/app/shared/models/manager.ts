export interface IManager {
  id: number;
  firstName: string;
  lastName: string;
  upManagerId: number;
  registrationDate: Date;
  soldItemsNumber: number;
  soldItemsPrice: number;
  workedOut: number;
  referalsWorkedOut: number;
  total: number;
  isDeleted: boolean;
}

export class ManagerFormValues {
  firstname = '';
  lastname = '';

  constructor(init?: ManagerFormValues) {
    Object.assign(this, init);
  }
}
