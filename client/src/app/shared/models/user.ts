export interface IUser {
    email: string;
    displayName: string;
    token: string;
    managerId: number;
}
