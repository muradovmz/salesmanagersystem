import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/';
import { AccountService } from 'src/app/account/account.service';
import { IProduct } from 'src/app/shared/models/product';
import { IUser } from 'src/app/shared/models/user';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  itemform

  closeBtnName: string;
  title;
  data: IProduct;
  currentManagerId: number;

  public event: EventEmitter<any> = new EventEmitter();

  constructor(public modalRef: BsModalRef, private formBuilder: FormBuilder, private accountService: AccountService) {
  }



  ngOnInit(): void {
    this.accountService.currentUser$.subscribe((user: IUser) => {
      this.currentManagerId = user.managerId;
    }, error => { console.log(error); });

    this.itemform = this.formBuilder.group({
      quantity: [this.data.quantity.toString(), [Validators.required]],
      managerId: this.currentManagerId
    });

  }

 sellProduct(form) {
    if (form.value) {
      this.triggerEvent(form.value);
      this.modalRef.hide();
    }
  }

  triggerEvent(values: any) {
    this.event.emit(values);
  }
}