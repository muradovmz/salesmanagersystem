import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductAddEditComponent } from './product-add-edit/product-add-edit.component';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [ProductsComponent, ProductItemComponent, ProductAddEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ProductsRoutingModule,
    ModalModule.forRoot()
  ],
  entryComponents: [ProductAddEditComponent,ProductItemComponent]
})
export class ProductsModule { }
