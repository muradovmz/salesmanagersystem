import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPagination } from '../shared/models/pagination';
import { map } from 'rxjs/operators';
import { ProductParams } from '../shared/models/productParams';
import { IProduct } from '../shared/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  baseUrl = 'https://localhost:5001/api/';

  constructor(private http: HttpClient) { }

  getProducts(productParams: ProductParams) {
    let params = new HttpParams();

    if (productParams.sort) {
      params = params.append('sort', productParams.sort);
    }

    if (productParams.search) {
      params = params.append('search', productParams.search);
    }

    if (productParams.priceFrom) {
      params = params.append('priceFrom', productParams.priceFrom);
    }

    if (productParams.priceTo) {
      params = params.append('priceTo', productParams.priceTo);
    }

    params = params.append('sort', productParams.sort);
    params = params.append('pageIndex', productParams.pageNumber.toString());
    params = params.append('pageSize', productParams.pageSize.toString());

    return this.http.get<IPagination>(this.baseUrl + 'products', { observe: 'response', params })
      .pipe(
        map(response => {
          return response.body;
        })
      );
  }

  getProduct(id: number) {
    return this.http.get<IProduct>(this.baseUrl + 'products/' + id);
  }

  createProduct(product: IProduct) {
    return this.http.post(this.baseUrl + 'products', product);
  }

  updateProduct(product: any, id: number) {
    return this.http.put(this.baseUrl + 'products/' + id, product);
  }

  deleteProduct(id: number, executerId: number) {
    let params = new HttpParams();
    params = params.append('id', id.toString());
    params = params.append('executerId', executerId.toString());
    return this.http.delete(this.baseUrl + 'products/' + id, { observe: 'response', params })
      .pipe(
        map(response => {
          return response.body;
        })
      );
  }

  sellProduct(product: any, id: number) {
    return this.http.put(this.baseUrl + 'products/sell/' + id, product);
  }
}
