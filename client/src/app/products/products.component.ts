import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IProduct } from '../shared/models/product';
import { ProductParams } from '../shared/models/productParams';
import { ProductsService } from './products.service';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProductAddEditComponent } from './product-add-edit/product-add-edit.component';
import { AccountService } from '../account/account.service';
import { IUser } from '../shared/models/user';
import { ProductItemComponent } from './product-item/product-item.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @ViewChild('search', { static: true }) searchTerm: ElementRef;
  @ViewChild('priceFrom', { static: true }) priceFromTerm: ElementRef;
  @ViewChild('priceTo', { static: true }) priceToTerm: ElementRef;
  products: IProduct[];
  productParams = new ProductParams();
  totalCount: number;
  sortOptions = [
    { name: 'Alphabetical', value: 'name' },
    { name: 'Price: Low to High', value: 'priceAsc' },
    { name: 'Price: High to Low', value: 'priceDesc' },
    { name: 'Quantity: Low to High', value: 'quantityAsc' },
    { name: 'Quantity: High to Low', value: 'quantityDesc' }
  ];
  selectedItem = this.productParams.sort;

  modalRef: BsModalRef;

  currentManagerId: number;

  constructor(private productsService: ProductsService, private modalService: BsModalService, private accountService: AccountService) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe((user: IUser) => { this.currentManagerId = user.managerId; },
      error => { console.log(error); });
    this.getProducts();
  }

  getProducts() {
    this.productsService.getProducts(this.productParams).subscribe(response => {
      this.products = response.data;
      this.productParams.pageNumber = response.pageIndex;
      this.productParams.pageSize = response.pageSize;
      this.totalCount = response.count;
    }, error => {
      console.log(error);
    });
  }

  onSortSelected(sort: string) {
    this.productParams.sort = sort;
    this.getProducts();
  }

  onPageChanged(event: any) {
    if (this.productParams.pageNumber !== event) {
      this.productParams.pageNumber = event;
      this.getProducts();
    }
  }

  onSearch() {
    this.productParams.search = this.searchTerm.nativeElement.value;
    this.productParams.priceFrom = this.priceFromTerm.nativeElement.value;
    this.productParams.priceTo = this.priceToTerm.nativeElement.value;
    this.productParams.pageNumber = 1;
    this.getProducts();
  }

  onReset() {
    this.searchTerm.nativeElement.value = '';
    this.priceFromTerm.nativeElement.value = '';
    this.priceToTerm.nativeElement.value = '';
    this.productParams = new ProductParams();
    this.selectedItem = this.productParams.sort;
    this.getProducts();
  }

  openModal() {
    this.modalRef = this.modalService.show(ProductAddEditComponent, {
      initialState: {
        title: 'Add Product'
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
    this.modalRef.content.event.subscribe(res => {
      this.productsService.createProduct(res)
        .subscribe(response => {
          this.getProducts();
        }, error => {
          console.log(error);
        });
    });
  }

  openEditModal(product: IProduct) {
    this.modalRef = this.modalService.show(ProductAddEditComponent, {
      initialState: {
        title: 'Edit Product',
        data: product
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
    this.modalRef.content.event.subscribe(res => {
      this.productsService.updateProduct(res, product.id)
        .subscribe(response => {
          this.getProducts();
        }, error => {
          console.log(error);
        });
    });
  }


  deleteProduct(id: number) {
    this.productsService.deleteProduct(id, this.currentManagerId).subscribe((response: any) => {
      this.products.splice(this.products.findIndex(p => p.id === id), 1);
      this.totalCount--;
    }, error => {
      console.log(error);
    });
  }

  openSellModal(product: IProduct) {
    this.modalRef = this.modalService.show(ProductItemComponent, {
      initialState: {
        title: 'Sell Product',
        data: product
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
    this.modalRef.content.event.subscribe(res => {
      this.productsService.sellProduct(res, product.id)
        .subscribe(response => {
          this.getProducts();
        }, error => {
          console.log(error);
        });
    });
  }
}
