import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/';
import { AccountService } from 'src/app/account/account.service';
import { IProduct } from 'src/app/shared/models/product';
import { IUser } from 'src/app/shared/models/user';

@Component({
  selector: 'app-product-add-edit',
  templateUrl: './product-add-edit.component.html',
  styleUrls: ['./product-add-edit.component.scss']
})
export class ProductAddEditComponent implements OnInit {

  itemform

  closeBtnName: string;
  title;
  data: IProduct;
  currentManagerId: number;

  public event: EventEmitter<any> = new EventEmitter();

  constructor(public modalRef: BsModalRef, private formBuilder: FormBuilder, private accountService: AccountService) {
  }



  ngOnInit(): void {
    this.accountService.currentUser$.subscribe((user: IUser) => {
      this.currentManagerId = user.managerId;
    }, error => { console.log(error); });
    if (this.data) {
      this.itemform = this.formBuilder.group({
        name: [this.data.name, [Validators.required]],
        quantity: [this.data.quantity.toString(), [Validators.required]],
        price: [this.data.price.toString(), [Validators.required]],
        managerId: this.currentManagerId
      });
    } else {
      this.itemform = this.formBuilder.group({
        name: [null, [Validators.required]],
        quantity: [null, [Validators.required]],
        price: [null, [Validators.required]],
        managerId: this.currentManagerId
      });
    }
  }

  addAndSaveProduct(form) {
    if (form.value) {
      this.triggerEvent(form.value);
      this.modalRef.hide();
    }
  }

  triggerEvent(values: any) {
    this.event.emit(values);
  }
}
