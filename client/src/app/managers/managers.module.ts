import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagersComponent } from './managers.component';
import { SharedModule } from '../shared/shared.module';
import { ManagersRoutingModule } from './managers-routing.module';
import { FormsModule } from '@angular/forms';
import { ManagerAddComponent } from './manager-add/manager-add.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ManagerEditComponent } from './manager-edit/manager-edit.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';



@NgModule({
  declarations: [ManagersComponent, ManagerAddComponent, ManagerEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    ManagersRoutingModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  entryComponents: [ManagerAddComponent]
})
export class ManagersModule { }
