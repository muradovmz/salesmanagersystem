import { Component, EventEmitter, OnInit } from '@angular/core';
import { AsyncValidatorFn, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/';
import { of, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { AccountService } from 'src/app/account/account.service';
import { IUser } from 'src/app/shared/models/user';

@Component({
  selector: 'app-manager-add',
  templateUrl: './manager-add.component.html',
  styleUrls: ['./manager-add.component.scss']
})
export class ManagerAddComponent implements OnInit {

  registerForm: FormGroup;
  AppUserEmail: string;
  errors: string[];

  closeBtnName: string;
  title;
  data;

  public event: EventEmitter<any> = new EventEmitter();

  constructor(public modalRef: BsModalRef, private fb: FormBuilder, private accountService: AccountService) {
  }



  ngOnInit(): void {
    this.accountService.currentUser$.subscribe((ragaca: IUser) => { this.AppUserEmail = ragaca.email; }, error => { console.log(error); });
    this.registerForm = this.fb.group({
      displayName: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')],
        [this.validateEmailNotTaken()]],
      password: [null, [Validators.required]],
      appUserEmail: this.AppUserEmail
    });
  }

  createManager(registerForm) {
    if (registerForm.value) {
      this.triggerEvent(registerForm.value);
      this.modalRef.hide();
    }
  }

  triggerEvent(values: any) {
    this.event.emit(values);
  }
  validateEmailNotTaken(): AsyncValidatorFn {
    return control => {
      return timer(500).pipe(
        switchMap(() => {
          if (!control.value) {
            return of(null);
          }
          return this.accountService.checkEmailExists(control.value).pipe(
            map(res => {
              return res ? { emailExists: true } : null;
            })
          );
        })
      );
    };
  }
}
