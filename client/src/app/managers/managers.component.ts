import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IManager } from '../shared/models/manager';
import { ManagerParams } from '../shared/models/managerParams';
import { ManagersService } from './managers.service';


import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ManagerAddComponent } from './manager-add/manager-add.component';
import { AccountService } from '../account/account.service';
import { IUser } from '../shared/models/user';
import { ManagerEditComponent } from './manager-edit/manager-edit.component';

@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.scss']
})
export class ManagersComponent implements OnInit {
  @ViewChild('searchByFirstName', { static: true }) searchByFirstNameTerm: ElementRef;
  @ViewChild('searchByLastName', { static: true }) searchLastnameTerm: ElementRef;
  @ViewChild('soldItemNumberFrom', { static: true }) soldItemNumberFromTerm: ElementRef;
  @ViewChild('soldItemNumberTo', { static: true }) soldItemNumberToTerm: ElementRef;
  @ViewChild('soldItemPriceFrom', { static: true }) soldItemPriceFromTerm: ElementRef;
  @ViewChild('soldItemPriceTo', { static: true }) soldItemPriceToTerm: ElementRef;
  @ViewChild('workedOutFrom', { static: true }) workedOutFromTerm: ElementRef;
  @ViewChild('workedOutTo', { static: true }) workedOutToTerm: ElementRef;
  @ViewChild('referalsWorkedOutFrom', { static: true }) referalsWorkedOutFromTerm: ElementRef;
  @ViewChild('referalsWorkedOutTo', { static: true }) referalsWorkedOutToTerm: ElementRef;
  @ViewChild('totalFrom', { static: true }) totalFromTerm: ElementRef;
  @ViewChild('totalTo', { static: true }) totalToTerm: ElementRef;
  @ViewChild('registeredFrom', { static: true }) registeredFromTerm: ElementRef;
  @ViewChild('registeredTo', { static: true }) registeredToTerm: ElementRef;

  managers: IManager[];
  managerParams = new ManagerParams();
  totalCount: number;
  sortOptions = [
    { name: 'Alphabetical Firstname', value: 'firstnameAsc' },
    { name: 'Alphabetical Lastname', value: 'lastnameAsc' },
    { name: 'SoldItemNumber: Low to High', value: 'soldItemsNumberAsc' },
    { name: 'SoldItemNumber: High to Low', value: 'soldItemsNumberDesc' },
    { name: 'SoldItemPrice: Low to High', value: 'soldItemsPriceAsc' },
    { name: 'SoldItemPrice: High to Low', value: 'soldItemsPriceDesc' },
    { name: 'WorkedOut: Low to High', value: 'workedOutAsc' },
    { name: 'WorkedOut: High to Low', value: 'workedOutDesc' },
    { name: 'ReferalsWorkedOut: Low to High', value: 'referalsWorkedOutAsc' },
    { name: 'ReferalsWorkedOut: High to Low', value: 'referalsWorkedOutDesc' },
    { name: 'Total: Low to High', value: 'totalAsc' },
    { name: 'Total: High to Low', value: 'totalDesc' }
  ];
  selectedItem = this.managerParams.sort;
  modalRef: BsModalRef;
  currentManagerId: number;

  constructor(private managersService: ManagersService, private modalService: BsModalService, private accountService: AccountService) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe((user: IUser) => { this.currentManagerId = user.managerId; },
      error => { console.log(error); });
    this.getManagers();
  }

  getManagers() {
    this.managersService.getManagers(this.managerParams).subscribe(response => {
      this.managers = response.data;
      this.managerParams.pageNumber = response.pageIndex;
      this.managerParams.pageSize = response.pageSize;
      this.totalCount = response.count;
    }, error => {
      console.log(error);
    });
  }

  onSortSelected(sort: string) {
    this.managerParams.sort = sort;
    this.getManagers();
  }

  onPageChanged(event: any) {
    if (this.managerParams.pageNumber !== event) {
      this.managerParams.pageNumber = event;
      this.getManagers();
    }
  }

  onSearch() {
    this.managerParams.searchByFirstName = this.searchByFirstNameTerm.nativeElement.value;
    this.managerParams.searchByLastName = this.searchLastnameTerm.nativeElement.value;
    this.managerParams.soldItemNumberFrom = this.soldItemNumberFromTerm.nativeElement.value;
    this.managerParams.soldItemNumberTo = this.soldItemNumberToTerm.nativeElement.value;
    this.managerParams.soldItemPriceFrom = this.soldItemPriceFromTerm.nativeElement.value;
    this.managerParams.soldItemPriceTo = this.soldItemPriceToTerm.nativeElement.value;
    this.managerParams.workedOutFrom = this.workedOutFromTerm.nativeElement.value;
    this.managerParams.workedOutTo = this.workedOutToTerm.nativeElement.value;
    this.managerParams.referalsWorkedOutFrom = this.referalsWorkedOutFromTerm.nativeElement.value;
    this.managerParams.referalsWorkedOutTo = this.referalsWorkedOutToTerm.nativeElement.value;
    this.managerParams.totalFrom = this.totalFromTerm.nativeElement.value;
    this.managerParams.totalTo = this.totalToTerm.nativeElement.value;
    this.managerParams.registeredFrom = this.registeredFromTerm.nativeElement.value;
    this.managerParams.registeredTo = this.registeredToTerm.nativeElement.value;

    this.managerParams.pageNumber = 1;
    this.getManagers();
  }

  onReset() {
    this.searchByFirstNameTerm.nativeElement.value = '';
    this.searchLastnameTerm.nativeElement.value = '';
    this.soldItemNumberFromTerm.nativeElement.valu = '';
    this.soldItemNumberToTerm.nativeElement.value = '';
    this.soldItemPriceFromTerm.nativeElement.value = '';
    this.soldItemPriceToTerm.nativeElement.value = '';
    this.workedOutFromTerm.nativeElement.value = '';
    this.workedOutToTerm.nativeElement.value = '';
    this.referalsWorkedOutFromTerm.nativeElement.value = '';
    this.referalsWorkedOutToTerm.nativeElement.value = '';
    this.totalFromTerm.nativeElement.value = '';
    this.totalToTerm.nativeElement.value = '';
    this.registeredFromTerm.nativeElement.value = '';
    this.registeredToTerm.nativeElement.value = '';

    this.managerParams = new ManagerParams();
    this.selectedItem = this.managerParams.sort;
    this.getManagers();
  }


  openModal() {
    this.modalRef = this.modalService.show(ManagerAddComponent, {
      initialState: {
        title: 'Add Manager',
        data: {}
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
    this.modalRef.content.event.subscribe(res => {
      this.accountService.register(res)
        .subscribe(response => {
          this.getManagers();
        }, error => {
          console.log(error);
        });
    });
  }

  openEditModal(id) {
    this.modalRef = this.modalService.show(ManagerEditComponent, {
      initialState: {
        title: 'Edit Manager'
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
    this.modalRef.content.event.subscribe(res => {
      this.managersService.updateManager(res, id, this.currentManagerId)
        .subscribe(response => {
          this.getManagers();
        }, error => {
          console.log(error);
        });
    });
  }

  deleteManager(id: number) {
    this.managersService.deleteManager(id, this.currentManagerId).subscribe((response: any) => {
      this.managers.splice(this.managers.findIndex(p => p.id === id), 1);
      this.totalCount--;
    }, error => {
      console.log(error);
    });
  }
}
