import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPagination } from '../shared/models/pagination';
import { map } from 'rxjs/operators';
import { ManagerParams } from '../shared/models/managerParams';
import { IManager, ManagerFormValues } from '../shared/models/manager';

@Injectable({
  providedIn: 'root'
})
export class ManagersService {
  baseUrl = 'https://localhost:5001/api/';

  constructor(private http: HttpClient) { }

  getManagers(managerParams: ManagerParams) {
    let params = new HttpParams();

    if (managerParams.sort) {
      params = params.append('sort', managerParams.sort);
    }

    if (managerParams.searchByFirstName) {
      params = params.append('searchFirstName', managerParams.searchByFirstName);
    }
    if (managerParams.searchByLastName) {
      params = params.append('searchLastLame', managerParams.searchByLastName);
    }
    if (managerParams.soldItemNumberFrom) {
      params = params.append('soldItemNumberFrom', managerParams.soldItemNumberFrom);
    }
    if (managerParams.soldItemNumberTo) {
      params = params.append('soldItemNumberTo', managerParams.soldItemNumberTo);
    }
    if (managerParams.soldItemPriceFrom) {
      params = params.append('soldItemPriceFrom', managerParams.soldItemPriceFrom);
    }
    if (managerParams.soldItemPriceTo) {
      params = params.append('soldItemPriceTo', managerParams.soldItemPriceTo);
    }
    if (managerParams.workedOutFrom) {
      params = params.append('workedOutFrom', managerParams.workedOutFrom);
    }
    if (managerParams.workedOutTo) {
      params = params.append('workedOutTo', managerParams.workedOutTo);
    }
    if (managerParams.referalsWorkedOutFrom) {
      params = params.append('referalsWorkedOutFrom', managerParams.referalsWorkedOutFrom);
    }
    if (managerParams.referalsWorkedOutTo) {
      params = params.append('referalsWorkedOutTo', managerParams.referalsWorkedOutTo);
    }
    if (managerParams.totalFrom) {
      params = params.append('TotalFrom', managerParams.totalFrom);
    }
    if (managerParams.totalTo) {
      params = params.append('TotalTo', managerParams.totalTo);
    }
    if (managerParams.registeredFrom) {
      params = params.append('registeredFrom', managerParams.registeredFrom);
    }
    if (managerParams.searchByFirstName) {
      params = params.append('registeredTo', managerParams.registeredTo);
    }

    params = params.append('sort', managerParams.sort);
    params = params.append('pageIndex', managerParams.pageNumber.toString());
    params = params.append('pageSize', managerParams.pageSize.toString());

    return this.http.get<IPagination>(this.baseUrl + 'managers', { observe: 'response', params })
      .pipe(
        map(response => {
          return response.body;
        })
      );
  }

  getManager(id: number) {
    return this.http.get<IManager>(this.baseUrl + 'managers/' + id);
  }

  updateManager(manager: any, id: number, executerId: number) {
    let params = new HttpParams();
    params = params.append('executerId', executerId.toString());
    return this.http.put(this.baseUrl + 'managers/' + id, manager, { observe: 'response', params });
  }

  deleteManager(id: number, executerId: number) {
    let params = new HttpParams();
    params = params.append('id', id.toString());
    params = params.append('executerId', executerId.toString());
    return this.http.delete(this.baseUrl + 'managers/' + id, { observe: 'response', params })
      .pipe(
        map(response => {
          return response.body;
        })
      );
  }


}
