import { Component, EventEmitter, OnInit } from '@angular/core';
import { AsyncValidatorFn, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/';

@Component({
  selector: 'app-manager-edit',
  templateUrl: './manager-edit.component.html',
  styleUrls: ['./manager-edit.component.scss']
})
export class ManagerEditComponent implements OnInit {

  editForm: FormGroup;
  title;
  closeBtnName: string;

  public event: EventEmitter<any> = new EventEmitter();

  constructor(public modalRef: BsModalRef, private fb: FormBuilder, ) {
  }

  ngOnInit(): void {
    this.editForm = this.fb.group({
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]]
    });
  }

  editManager(editForm) {
    if (editForm.value) {
      this.triggerEvent(editForm.value);
      this.modalRef.hide();
    }
  }

  triggerEvent(values: any) {
    this.event.emit(values);
  }

}
