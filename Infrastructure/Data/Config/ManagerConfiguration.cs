using Core.Entities;
using Core.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Config
{
    public class ManagerConfiguration : IEntityTypeConfiguration<Manager>
    {
        public void Configure(EntityTypeBuilder<Manager> builder)
        {
            builder.Property(p=>p.Id).IsRequired();
            builder.Property(p=>p.FirstName).IsRequired().HasMaxLength(150);
            builder.Property(p=>p.LastName).IsRequired().HasMaxLength(150);
            builder.HasOne(b => b.UpManager).WithMany(m => m.SubManagers).HasForeignKey(b => b.UpManagerId).IsRequired(false).OnDelete(DeleteBehavior.Restrict);
            builder.Property(v => v.RegistrationDate).IsRequired();
            builder.Property(v => v.SoldItemsNumber).IsRequired();
            builder.Property(p=>p.SoldItemsPrice).HasColumnType("decimal(18,2)");
            builder.Property(p=>p.WorkedOut).HasColumnType("decimal(18,2)");
            builder.Property(p=>p.ReferalsWorkedOut).HasColumnType("decimal(18,2)");
        }
    }
}