using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Identity;
using Core.Interfaces;
using Infrastructure.Data;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Identity
{
    public class AppIdentityDbContextSeed
    {
        private readonly StoreContext _context;
        public AppIdentityDbContextSeed(StoreContext context)
        {
            _context = context;
        }

        public static async Task SeedUsersAsync(UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var user = new AppUser
                {
                    DisplayName = "Admin",
                    Email = "admin@test.ge",
                    UserName = "admin@test.ge"
                };

                await userManager.CreateAsync(user, "Pa$$w0rd");
            }
        }
    }
}